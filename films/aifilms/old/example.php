<html>
<head>
<meta name="description" content="MIT CSAIL Film History of AI Database">
<meta name="keywords" content="CSAIL, MIT, MIT CSAIL, Film History of AI, History of AI, AI, MIT AI, Artificial Intelligence">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title> MIT Film History of Artificial Intelligence </title>
<link rel="stylesheet" type="text/css" href="style.css"/>
</head>

<body>
<!-- Table for Main Body -->
<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="2">
<tr>
<th valign="top" align="left" bgcolor="#202020" width="90" rowspan="2">
<p>
<p>
</th>

<th width="1" bgcolor="#CC0033" valign="left" rowspan="2" >  </th>

<td bgcolor="grey" >
<center>
<h1> <font ="verdana"> MIT Film History of Artificial Intelligence</font> </h1>

</td>
</tr>

<tr>
<td valign="top">




<center>

<h2><font color ="#CC0033"> #66 Turn Off Rug / Boy on Bongo BD </font> </h2>

<img align="center" src="bongo.jpg">

<p> <b>duration:</b> 3min
<br> <b>year:</b> <i> unknown </i>
<br> <b>mpeg:</b> <a href="digitalFilms/7mpeg/60-bongo.mpg">60-bongo.mpg</a>
<br> <b>mp4:</b> <a href="digitalFilms/7mp4/60-bongo.mp4">60-bongo.mp4</a>

<p> <b>Comments:</b> Camera original. Buckled.<br>		  

<p> <b>Further Information:</b><i> not available </i><br>		  


<p>
<br>
<font color="#CC0033" size="1">
<i>Do you know more about this film?  
<br>Submit details to the AIFilms 
<a href="http://www.editthis.info/aifilms/Main_Page">
<font color="#CC0033" size="2">wiki</font></a>
</font>

<br>


<br>

<center>
<table align="center">
<tr>
<th><a href="http://web.mit.edu"><img border="0" src="http://web.mit.edu/img/d060504-logo.gif"></a>
</th>

<th width="20"></th>

<th><a  href="http://nsf.gov"><img border="0" src="http://www.soest.hawaii.edu/seagrant/MSURF_website/www/firework/nsf.gif"> </a></th>
</tr>
</table>

 <a href="mailto:tjg@csail.mit.edu"><font size="1" color="black">TJG</font></a>



</td>

</tr></table>

</body></html>