@filesa = split(/\n/, `cat filenamelist.txt`);

print '<center>\n<div align="center">\n<table width="90%" text-align="center" border="0" cellspacing="2" cellpadding="2">\n<tbody>';

foreach $filename (@filesa) {
    print "<tr>\n<td width=\"20%\">\n";
    print "<a href=\"http://projects.csail.mit.edu/films/Podcasts/$filename.mp3\">$filename.mp3</a>";
    print "\n</td>\n<td width=\"10%\">\n</td>\n<td width=\"70%\">\n";
    $description = system("cat Podcasts/Submit/$filename.txt");
    print "\n</td>\n</tr>";
}

print "</tbody>\n</table>\n</div>\n</center>";
