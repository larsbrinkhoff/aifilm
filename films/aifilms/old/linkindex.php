<html>
<head>
<meta name="description" content="MIT CSAIL Film History of AI Database">
<meta name="keywords" content="CSAIL, MIT, MIT CSAIL, Film History of AI, History of AI, AI, MIT AI, Artificial Intelligence">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title> MIT Film History of Artificial Intelligence </title>
<link rel="stylesheet" type="text/css" href="style.css"/>
</head>

<body>
<!-- Table for Main Body -->
<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="2">
<tr>
<td valign="top" align="left" bgcolor="#202020" width="90" rowspan="2">
<p>
<p>
</td>

<td width="1" bgcolor="#CC0033" valign="top" rowspan="2" >  </td>

<td valign="top">
<center>
<h1> MIT Film History of Artificial Intelligence </h1>

<hr>

<h2> <center> Index By Film Number</center>
<p>
<table style="width: 100%; text-align: center; margin-left: auto; margin-right: auto;" border="0" cellpadding="0" cellspacing="2">
<tbody>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=01">01-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=20">20-cube</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=39">39-worm</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=58">58-logo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=77">77-life</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=02">02-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=21">21-bongo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=40">40-logo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=59">59-bongo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=78">78-cube</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=03">03-gas</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=22">22-turtle</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=41">41-radial</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=60">60-bongo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=79">79-logo</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=04">04-cubechess</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=23">23-worm</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=42">42-radial</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=61">61-arm</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=80">80-cube</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=05">05-life</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=24">24-worm</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=43">43-arm</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=62">62-logo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=81">81-mathlab</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=06">06-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=25">25-logo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=44">44-logo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=63">63-chess</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=82">82-screen</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=07">07-arm</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=26">26-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=45">45-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=64">64-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=83">83-logo</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=08">08-turtle</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=27">27-solar</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=46">46-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=65">65-worm</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=84">84-robot</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=09">09-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=28">28-logo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=47">47-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=66">66-worm</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=85">85-screen</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=10">10-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=29">29-solar</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=48">48-worm</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=67">67-life</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=86">86-bongo</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=11">11-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=30">30-gas</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=49">49-worm</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=68">68-gas</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=87">87-cube</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=12">12-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=31">31-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=50">50-logo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=69">69-gas</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=88">88-eye</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=13">13-cubechess</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=32">32-mathlab</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=51">51-cube</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=70">70-gas</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=89">89-logo</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=14">14-cube</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=33">33-cube</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=52">52-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=71">71-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=90">90-logo</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=15">15-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=34">34-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=53">53-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=72">72-gas</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=91">91-logo</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=16">16-solar</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=35">35-screen</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=54">54-bongo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=73">73-gas</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=92">92-turtle</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=17">17-turtle</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=36">36-wireforks</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=55">55-worm</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=74">74-bongo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=93">93-cube</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=18">18-bongo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=37">37-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=56">56-life</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=75">75-logo</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=94">94-solar</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=19">19-cube</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=38">38-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=57">57-screen</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=76">76-robot</a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=95">95-turtle</a></td>
</tr>
<tr>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23="></a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23="></a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23="></a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23="></a></td>
<td><a href="http://projects.csail.mit.edu/films/aifilmsdynamicPage6.php?reel%23=96">96-worm</a></td>
</tr>
<tr>
</tbody>
</table>
</h2>

<p>

</center>

<br>

<center>
<table align="center">
<tr>
<th><a href="http://web.mit.edu"><img border="0" src="http://web.mit.edu/img/d060504-logo.gif"></a>
</th>

<th width="20"></th>

<th><a  href="http://nsf.gov"><img border="0" src="http://www.soest.hawaii.edu/seagrant/MSURF_website/www/firework/nsf.gif"> </a></th>
</tr>
</table>


<center>
    <address>
    <a href="mailto:tjg@csail.mit.edu">TJG</a><!-- hhmts end -->
  </address>
</center>
</td>
<td width="1" bgcolor="lightskyblue" valign="top">   </td>
</tr></table>

</body></html>

