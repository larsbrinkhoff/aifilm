<center>

These are all the parts of the code to the SHRDLU program that Terry Winograd wrote as part of his thesis. <br><br>

<table text-align="center" cellspacing="4" cellpadding="0" border="0"><tbody><tr><td>
<a href="http://projects.csail.mit.edu/films/shrdlu/blockl.txt">blockl</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/blockp.txt">blockp</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/blurb.txt">blurb</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/cgram.txt">cgram</a>
</td>
</tr>
<tr><td>
<a href="http://projects.csail.mit.edu/films/shrdlu/data.txt">data</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/demo.txt">demo</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/dictio.txt">dictio</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/fasl.txt">fasl</a>
</td>
</tr>
<tr><td>
<a href="http://projects.csail.mit.edu/films/shrdlu/file-note.txt">file-note</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/files.txt">files</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/ginter.txt">ginter</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/gramar.txt">gramar</a>
</td>
</tr>
<tr><td>
<a href="http://projects.csail.mit.edu/films/shrdlu/help.txt">help</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/init.txt">init</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/lisp.txt">lisp</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/loader.txt">loader</a>
</td>
</tr>
<tr><td>
<a href="http://projects.csail.mit.edu/films/shrdlu/macros.txt">macros</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/mannew.txt">mannew</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/manual.txt">manual</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/minih.txt">minih</a>
</td>
</tr>
<tr><td>
<a href="http://projects.csail.mit.edu/films/shrdlu/morpho.txt">morpho</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/newans.txt">newans</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/parser.txt">parser</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/plnr.txt">plnr</a>
</td>
</tr>
<tr><td>
<a href="http://projects.csail.mit.edu/films/shrdlu/progmr.txt">progmr</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/setup.txt">setup</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/show.txt">show</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/smass.txt">smass</a>
</td>
</tr>
<tr><td>
<a href="http://projects.csail.mit.edu/films/shrdlu/smspec.txt">smspec</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/smutil.txt">smutil</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/syscom.txt">syscom</a>
</td>
<td>
<a href="http://projects.csail.mit.edu/films/shrdlu/thtrac.txt">thtrac</a>
</td>
</tr>
<tr></tr></tbody></table></center>
