<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>



<head>
	<meta name="description" content="MIT CSAIL Film History of AI Database">
	<meta name="keywords" content="CSAIL, MIT, MIT CSAIL, Film History of AI, History of AI, AI, MIT AI, Artificial 
Intelligence">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title> Oral Histories </title>
	<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<?php
	include ("head.html");
?>
<!-- Table for Main Body -->
<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="2">
	<tr>
		<th valign="top" align="left" bgcolor="#202020" width="90" rowspan="2">
			<p>
			<p>

<center>
<br><br><br><p><a href="index.php"><font size=1 color="#D3D3D3">Home</font></a>

<p><b><a href="paper.php"><font size=1 color="#D3D3D3">History</font></a></b>

<p><b><a href="searchPage.php"><font size=1 color="#D3D3D3">Search</font></a></b>

<p><b><a href="final/timelinewithformat.php"><font size=1 color="#D3D3D3">Timeline</font></a></b>

<p><b><a href="numberedIndexPage.php"><font size=1 color="#D3D3D3">By Number</font></a></b>

<p><b><a href="chronicledIndexPage.php"><font size=1 color="#D3D3D3">By Year</font></a></b>

<p><b><a href="categorizedIndexPage.php"><font size=1 color="#D3D3D3">By Category</font></a></b>

<p><b><a href="podcastindex.php"><font size=1 color="#D3D3D3">Podcasts</font></a></b>

<p><b><a href="oralhist.php"><font size=1 color="#D3D3D3">Oral Histories</font></a></b>

<br><p><b><a href="sources.php"><font size=1 color="#D3D3D3">Links</font></a></b>

<p><b><a href="textintro.php"><font size=1 color="#D3D3D3">Text</font></a></b>

</center>

		</th>

		<th width="1" bgcolor="#CC0033" valign="left" rowspan="2" >
		</th>

		<td bgcolor="#808080" >
			<center>
				<h2> <font ="verdana" color="#D3D3D3"> Early Artificial Intelligence Research : Caught on Film</font> </h2>
			</center>
		</td>
	</tr>

	<tr>
		<td valign="top">
		<center> <font color="#CC0033" size="6">
                                <b>Oral Histories</b></font><br>
		</center>

<center>
<b><a href="http://projects.csail.mit.edu/films/index.php"><font size=1>[Home]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/paper.php"><font size=1>[History]</font></a></b>


<b><a href="http://projects.csail.mit.edu/films/searchPage.php"><font size=1>[Search]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/final/timelinewithformat.php"><font size=1>[Timeline]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/numberedIndexPage.php"><font size=1>[By Number]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/chronicledIndexPage.php"><font size=1>[By Year]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/categorizedIndexPage.php"><font size=1>[By Category]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/podcastindex.php"><font size=1>[Podcasts]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/oralhist.php"><font size=1>[Oral Histories]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/sources.php"><font size=1>[Links]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/textintro.php"><font size=1>[Text]</font></a></b>


</center>

<p><center>
<table width=80% text-align="center" cellpadding="0" cellspacing="0" border="0">
<tbody>
<tr><td><center><p>
The following informal film interviews were conducted during the AAAI 50th Anniversary Celebration (Fellows Symposium) and during the summer of 2006.  
<p>

<br><br>
Ron Brachman: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Brachman.mp4">Brachman.mp4 &nbsp;&nbsp;  [93769 kB]</a><br>
Danny Bobrow: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Bobrow.mp4">Bobrow.mp4 &nbsp;&nbsp;  [107356 kB]</a><br>
Jim Hendler: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Hendler.mp4">Hendler.mp4  [59987 kB]&nbsp;&nbsp;</a><br>
Nils Nilsson: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Nilsson.mp4">Nilsson.mp4  &nbsp;&nbsp[131951 kB]</a><br>
David Wilkins: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Wilkins.mp4">Wilkins.mp4  &nbsp;&nbsp[72520 kB]</a><br>
Ben Kuipers:  <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Kuipers.mp4">Kuipers.mp4   [132268 kB]&nbsp;&nbsp;</a><br>
Manuela Veloso:  <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Veloso.mp4">Veloso.mp4  &nbsp;&nbsp[64227 kB]</a><br>
Ed Feigenbaum:  <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Feigenbaum.mp4">Feigenbaum.mp4  &nbsp;&nbsp;[93737 kB]</a><br>
Randall Davis:  <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Davis.mp4">Davis.mp4</a><br>
Harry Barrow: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Barrow.mp4">Barrow.mp4</a><br>
Bruce Buchanan: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Buchanan.mp4">Buchanan.mp4&nbsp;&nbsp;  [54199 kB]</a><br>
Alan Bundy: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Bundy.mp4">Bundy.mp4  &nbsp;&nbsp;[60636 kB]</a><br>
Jon Doyle: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Doyle.mp4">Doyle.mp4  &nbsp;&nbsp;[77911 kB]</a><br>
Drew McDermott: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/McDermott.mp4">McDermott.mp4  &nbsp;&nbsp[135558 kB]</a><br>
Ryszard Michalski: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Michalski.mp4">Michalski.mp4  &nbsp;&nbsp[80121 kB]</a><br>
Chuck Rich: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Rich.mp4">Rich.mp4  &nbsp;&nbsp[89196 kB]</a><br>
Edwina Rissland: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Rissland.mp4">Rissland.mp4  &nbsp;&nbsp[71319 kB]</a><br>
Candace Sidner: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Sidner.mp4">Sidner.mp4  &nbsp;&nbsp[39262 kB]</a><br>
Reid Simmons: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Simmons.mp4">Simmons.mp4  &nbsp;&nbsp[78877 kB]</a><br>
Gerry Sussman: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Sussman.mp4">Sussman.mp4  &nbsp;&nbsp[106306 kB]</a><br>
Beverly Woolf: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Woolf.mp4">Woolf.mp4  &nbsp;&nbsp[58215 kB]</a><br>
Peter Szolovits: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Szolovits.mp4">Szolovits.mp4  &nbsp;&nbsp[135730 kB]</a><br>
Henry Kautz: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Kautz.mp4">Kautz.mp4  [59350 kB]&nbsp;&nbsp;</a><br>
Bart Selman: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Selman.mp4">Selman.mp4  &nbsp;&nbsp[47752 kB]</a><br>
Bill Swartout: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Swartout.mp4">Swartout.mp4  &nbsp;&nbsp[78225 kB]</a><br>
Pat Winston: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Winston.mp4">Winston.mp4  &nbsp;&nbsp[64749 kB]</a><br>
Rod Brooks: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Brooks.mp4">Brooks.mp4&nbsp;&nbsp;  [103357 kB]</a><br>
Marvin Minsky: <a href="http://projects.csail.mit.edu/films/AAAIfootage/done/Minsky.mp4">Minsky.mp4  &nbsp;&nbsp[141748 kB]</a><br>
</center>
</td></tr>
</tbody></table>
</center>

<center>
<b><a href="http://projects.csail.mit.edu/films/index.php"><font size=1>[Home]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/paper.php"><font size=1>[History]</font></a></b>


<b><a href="http://projects.csail.mit.edu/films/searchPage.php"><font size=1>[Search]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/final/timelinewithformat.php"><font size=1>[Timeline]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/numberedIndexPage.php"><font size=1>[By Number]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/chronicledIndexPage.php"><font size=1>[By Year]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/categorizedIndexPage.php"><font size=1>[By Category]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/podcastindex.php"><font size=1>[Podcasts]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/oralhist.php"><font size=1>[Oral Histories]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/sources.php"><font size=1>[Links]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/textintro.php"><font size=1>[Text]</font></a></b>


</center>


			<br>
			<table width="100%" align="center">
				<tr>
					<th width="47%">
						<div align="right">
						<a href="http://web.mit.edu"><img border="0" src="http://web.mit.edu/img/d060504-logo.gif"></a>
						</div>
					</th>
					<th width="20">
					</th>
					<th width="20">
					</th>
					<th>
						<div align="left">
						<a href="http://nsf.gov"><img border="0" src="http://projects.csail.mit.edu/films/nsf.gif"></a>
						</div>
					</th>
				</tr>
			</table>
			<center> 
			<a href="mailto:tjg@csail.mit.edu"><font size="1" color="black">TJG</font></a>
			</center>
		</td>
	</tr>
</table>

</body>

</html>
