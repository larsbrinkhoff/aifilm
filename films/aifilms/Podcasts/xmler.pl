
@filesa = split(/\n/,`cat filenamelist.txt`);
foreach(@filesa) {
    $filename = $_;
    $description = `cat Submit/$filename.txt`;
    $description =~ s/Greenblath/Greenblatt/;
    $description =~ s/Bill Greenblatt/Rick Greenblatt/;
    $description =~ s/"\./\."/g;
    $description =~ s/"/'/g;
    $description =~ s/^M/" "/g;
    $date = `date`;
    @filenamea = split(/-/, $filename);
    @sizea = split(/\s+/,`ls -l Submit/ | grep @filenamea[0]- | grep mp3`);
    $size = @sizea[4];
    print "<item>\n<title>$filename</title>\n<description>";
    print "$description";
    print "</description>\n<pubDate>$date</pubDate>\n<enclosure url=\"http://projects.csail.mit.edu/films/Podcasts/$filename.mp3\" length=\"$size\" type=\"audio/mpeg\"/>\n</item>\n";

}
