<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>


<head>
	<meta name="description" content="MIT CSAIL Film History of AI Database">
	<meta name="keywords" content="CSAIL, MIT, MIT CSAIL, Film History of AI, History of AI, AI, MIT AI, Artificial 
Intelligence">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title> Recovering MIT's AI Film History </title>
	<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<?php
	include ("head.html");
?><!-- Table for Main Body -->
</head>






<body>
<table border="0" cellpadding="2" cellspacing="0" height="100%" width="100%">

	<tbody>
    <tr>

		<th rowspan="2" align="left" bgcolor="#202020" valign="top" width="90">
			
      <p>
			</p>
      <p>

<center>
<br><br><br><p><a href="index.php"><font size=1 color="#D3D3D3">Home</font></a>

<p><b><a href="paper.php"><font size=1 color="#D3D3D3">History</font></a></b>

<p><b><a href="searchPage.php"><font size=1 color="#D3D3D3">Search</font></a></b>

<p><b><a href="final/timelinewithformat.php"><font size=1 color="#D3D3D3">Timeline</font></a></b>

<p><b><a href="numberedIndexPage.php"><font size=1 color="#D3D3D3">By Number</font></a></b>

<p><b><a href="chronicledIndexPage.php"><font size=1 color="#D3D3D3">By Year</font></a></b>

<p><b><a href="categorizedIndexPage.php"><font size=1 color="#D3D3D3">By Category</font></a></b>

<p><b><a href="podcastindex.php"><font size=1 color="#D3D3D3">Podcasts</font></a></b>

<p><b><a href="oralhist.php"><font size=1 color="#D3D3D3">Oral Histories</font></a></b>

<br><p><b><a href="sources.php"><font size=1 color="#D3D3D3">Links</font></a></b>

<p><b><a href="textintro.php"><font size=1 color="#D3D3D3">Text</font></a></b>

</center>

		</p>
      </th>


		<th rowspan="2" bgcolor="#cc0033" valign="left" width="1">
		</th>


		<td bgcolor="#808080">
			
      <center>
				
      <h2> <font ="verdana" color="#d3d3d3"> Early Artificial Intelligence Research : Caught on Film</font> </h2>

			</center>

		</td>

	</tr>


	<tr>

		<td valign="top">
		
      <center> <font color="#cc0033" size="6">
                                <b>Recovering MIT's AI Film History</b></font><br>


<center>
<b><a href="http://projects.csail.mit.edu/films/index.php"><font size=1>[Home]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/paper.php"><font size=1>[History]</font></a></b>


<b><a href="http://projects.csail.mit.edu/films/searchPage.php"><font size=1>[Search]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/final/timelinewithformat.php"><font size=1>[Timeline]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/numberedIndexPage.php"><font size=1>[By Number]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/chronicledIndexPage.php"><font size=1>[By Year]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/categorizedIndexPage.php"><font size=1>[By Category]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/podcastindex.php"><font size=1>[Podcasts]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/oralhist.php"><font size=1>[Oral Histories]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/sources.php"><font size=1>[Links]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/textintro.php"><font size=1>[Text]</font></a></b>


</center>

<center>
Browse films by <a href="categorizedIndexPage.php">Category</a>
or <a href="numberedIndexPage.php">Number</a><p>

<table border="0" width="80%" height="100%" cellspacing="0" cellpadding="2">
<tbody>
<tr>
<td>
<center>
<b>  
This project was funded by the National Science Foundation to showcase early Artificial Intelligence research. It began with a treasure trove of ninety-six film reels found in the old MIT TechSquare, which are available here in both mpg and mp4 formats.  It ended as an NSF project on 31 December 2006.  

<br><br>If you have more information, ideas or stories, please tell us <a href="http://projects.csail.mit.edu/films/commentpostPage.php?filename=general&amp;title=AI%20History%20or%20how%20to%20Improve%20this%20Website"> <font color="#CC0033"><i>here</i></font></a> or at the bottom of the relevant film's page.  Your input helps recapture and clarify this captivating field's intellectual history.  

<br><br>To add materials or for other issues, please contact <a href="mailto:jackc@csail.mit.edu">Jack Costanza</a>.  

</b>
</center>
</td>
</tr>
</tbody>
</table>
</center>
<center>
<b><a href="http://projects.csail.mit.edu/films/index.php"><font size=1>[Home]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/paper.php"><font size=1>[History]</font></a></b>


<b><a href="http://projects.csail.mit.edu/films/searchPage.php"><font size=1>[Search]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/final/timelinewithformat.php"><font size=1>[Timeline]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/numberedIndexPage.php"><font size=1>[By Number]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/chronicledIndexPage.php"><font size=1>[By Year]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/categorizedIndexPage.php"><font size=1>[By Category]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/podcastindex.php"><font size=1>[Podcasts]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/oralhist.php"><font size=1>[Oral Histories]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/sources.php"><font size=1>[Links]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/textintro.php"><font size=1>[Text]</font></a></b>


</center>

		<br>

		
      <table align="center" width="100%">

				<tbody>
          <tr>

					<th width="47%">
						
            <div align="right">
						<a href="http://web.mit.edu"><img src="http://web.mit.edu/img/d060504-logo.gif" border="0"></a>
						</div>

					</th>

					<th width="20">
					</th>

					<th width="20">
					</th>

					<th>
						
            <div align="left">
						<a href="http://nsf.gov"><img src="http://projects.csail.mit.edu/films/nsf.gif" border="0"></a>
						</div>

					</th>

				</tr>

			
        </tbody>
      </table>

			</center>

			
      <center> 
		<font color="black" size="1"><br>
Designed by Luis Gomez, Heather Knight <br>
 and Matt Peddie [2006]

			<br>
Email: <a href="mailto:jackc@csail.mit.edu"><font color="black" size="1">Jack Costanza</font></a></font>
			</center>

		</td>

	</tr>

  </tbody>
</table>


</body>
</html>
