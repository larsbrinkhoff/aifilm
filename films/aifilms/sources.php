<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>



<head>
	<meta name="description" content="MIT CSAIL Film History of AI Database">
	<meta name="keywords" content="CSAIL, MIT, MIT CSAIL, Film History of AI, History of AI, AI, MIT AI, Artificial 
Intelligence">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title> More Sources </title>
	<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<?php
	include ("head.html");
?>
<!-- Table for Main Body -->
<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="2">
	<tr>
		<th valign="top" align="left" bgcolor="#202020" width="90" rowspan="2">
			<p>
			<p>

<center>
<br><br><br><p><a href="index.php"><font size=1 color="#D3D3D3">Home</font></a>

<p><b><a href="paper.php"><font size=1 color="#D3D3D3">History</font></a></b>

<p><b><a href="searchPage.php"><font size=1 color="#D3D3D3">Search</font></a></b>

<p><b><a href="final/timelinewithformat.php"><font size=1 color="#D3D3D3">Timeline</font></a></b>

<p><b><a href="numberedIndexPage.php"><font size=1 color="#D3D3D3">By Number</font></a></b>

<p><b><a href="chronicledIndexPage.php"><font size=1 color="#D3D3D3">By Year</font></a></b>

<p><b><a href="categorizedIndexPage.php"><font size=1 color="#D3D3D3">By Category</font></a></b>

<p><b><a href="podcastindex.php"><font size=1 color="#D3D3D3">Podcasts</font></a></b>

<p><b><a href="oralhist.php"><font size=1 color="#D3D3D3">Oral Histories</font></a></b>

<br><p><b><a href="sources.php"><font size=1 color="#D3D3D3">Links</font></a></b>

<p><b><a href="textintro.php"><font size=1 color="#D3D3D3">Text</font></a></b>

</center>

		</th>

		<th width="1" bgcolor="#CC0033" valign="left" rowspan="2" >
		</th>

		<td bgcolor="#808080" >
			<center>
				<h2> <font ="verdana" color="#D3D3D3"> Early Artificial Intelligence Research : Caught on Film</font> </h2>
			</center>
		</td>
	</tr>

	<tr>
		<td valign="top">
		<center> <font color="#CC0033" size="6">
                                <b>More Sources</b></font><br>
		</center>
<center>
<b><a href="http://projects.csail.mit.edu/films/index.php"><font size=1>[Home]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/paper.php"><font size=1>[History]</font></a></b>


<b><a href="http://projects.csail.mit.edu/films/searchPage.php"><font size=1>[Search]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/final/timelinewithformat.php"><font size=1>[Timeline]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/numberedIndexPage.php"><font size=1>[By Number]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/chronicledIndexPage.php"><font size=1>[By Year]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/categorizedIndexPage.php"><font size=1>[By Category]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/podcastindex.php"><font size=1>[Podcasts]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/oralhist.php"><font size=1>[Oral Histories]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/sources.php"><font size=1>[Links]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/textintro.php"><font size=1>[Text]</font></a></b>


</center>

<p><table>
<tbody>
   <tr>
      <td width="30%">
      </td>
      <td width="65%">
      <p><b><i>Further Project Information:</i></b>

<br><strong><a href="http://nsf.gov/awardsearch/showAward.do?AwardNumber=0537285">NSF Abstract</a>  Description of NSF Award </strong>

<br><strong><a href="FilmList.php">Original Film List</a> Compiled from the Original Tech Square Film Reels</strong>



<p><b><i>AI Timelines:</i></b>

<br><strong><a href="time-jpg/AI%201945-85.jpg">Graphical Timeline</a> developed for this project</strong>

<br><a href="http://www.aaai.org/aitopics/bbhist.html#early"><b>AAAI</b></a> 

<br><a href="http://www.csail.mit.edu/timeline/timeline.php"><b>MIT Tech Square</b></a>



<p><i>Related MIT Class Information:</i>

<br><a href="http://courses.csail.mit.edu/6.803/schedule.html"> Readings/Assignments for 6.803</a>, Patrick Winston's class at MIT
<br><a href="http://ocw.mit.edu/OcwWeb/Electrical-Engineering-and-Computer-Science/6-871Spring-2005/Readings/index.htm">6.871 Reading List</a>
<br><a href="http://ocw.mit.edu/OcwWeb/Electrical-Engineering-and-Computer-Science/index.htm">OCW Computer Science classes</a>

<p><i>Tape Recording:</i>
<br><a href="Audio/IJCAI%2025th.mp3">Audio Tape</a> from The 25th Anniversary (1981)IJCAI panel,McCarthy, Minsky, Samuel, Selfridge, Newell

      </td>
      <td width="5%">
      </td>
   </tr>
</tbody>
</table>
<center>
<b><a href="http://projects.csail.mit.edu/films/index.php"><font size=1>[Home]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/paper.php"><font size=1>[History]</font></a></b>


<b><a href="http://projects.csail.mit.edu/films/searchPage.php"><font size=1>[Search]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/final/timelinewithformat.php"><font size=1>[Timeline]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/numberedIndexPage.php"><font size=1>[By Number]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/chronicledIndexPage.php"><font size=1>[By Year]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/categorizedIndexPage.php"><font size=1>[By Category]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/podcastindex.php"><font size=1>[Podcasts]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/oralhist.php"><font size=1>[Oral Histories]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/sources.php"><font size=1>[Links]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/textintro.php"><font size=1>[Text]</font></a></b>


</center>


			<br>
			<table width="100%" align="center">
				<tr>
					<th width="47%">
						<div align="right">
						<a href="http://web.mit.edu"><img border="0" src="http://web.mit.edu/img/d060504-logo.gif"></a>
						</div>
					</th>
					<th width="20">
					</th>
					<th width="20">
					</th>
					<th>
						<div align="left">
						<a href="http://nsf.gov"><img border="0" src="http://projects.csail.mit.edu/films/nsf.gif"></a>
						</div>
					</th>
				</tr>
			</table>
			<center> 
			<a href="mailto:tjg@csail.mit.edu"><font size="1" color="black">TJG</font></a>
			</center>
		</td>
	</tr>
</table>

</body>

</html>
