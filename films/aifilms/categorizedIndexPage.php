<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>



<head>
	<meta name="description" content="MIT CSAIL Film History of AI Database">
	<meta name="keywords" content="CSAIL, MIT, MIT CSAIL, Film History of AI, History of AI, AI, MIT AI, Artificial 
Intelligence">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title> Categorized Film Index </title>
	<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<?php
	include ("head.html");
?>
<!-- Table for Main Body -->
<table text-align="center" align="center" border="0" width="100%" height="100%" cellspacing="0" cellpadding="2">
<tbody>
	<tr>
		<td valign="top" align="left" bgcolor="#202020" width="90" rowspan="2">
			<p>
			<p>

<center>
<br><br><br><p><a href="index.php"><font size=1 color="#D3D3D3">Home</font></a>

<p><b><a href="paper.php"><font size=1 color="#D3D3D3">History</font></a></b>

<p><b><a href="searchPage.php"><font size=1 color="#D3D3D3">Search</font></a></b>

<p><b><a href="final/timelinewithformat.php"><font size=1 color="#D3D3D3">Timeline</font></a></b>

<p><b><a href="numberedIndexPage.php"><font size=1 color="#D3D3D3">By Number</font></a></b>

<p><b><a href="chronicledIndexPage.php"><font size=1 color="#D3D3D3">By Year</font></a></b>

<p><b><a href="categorizedIndexPage.php"><font size=1 color="#D3D3D3">By Category</font></a></b>

<p><b><a href="podcastindex.php"><font size=1 color="#D3D3D3">Podcasts</font></a></b>

<p><b><a href="oralhist.php"><font size=1 color="#D3D3D3">Oral Histories</font></a></b>

<br><p><b><a href="sources.php"><font size=1 color="#D3D3D3">Links</font></a></b>

<p><b><a href="textintro.php"><font size=1 color="#D3D3D3">Text</font></a></b>

</center>

		</td>

		<td width="1" bgcolor="#CC0033" valign="left" rowspan="2" >
		</td>

		<td bgcolor="#808080" >
			<center>
				<h2> <font ="verdana" color="#D3D3D3"> Early Artificial Intelligence Research : Caught on Film</font> </h2>
			</center>
		</td>
	</tr>

	<tr>
		<td valign="top">
		<center> <font color="#CC0033" size="6">
                                <b>Categorized Film Index</b></font><br>
		</center>
<center>
<b><a href="http://projects.csail.mit.edu/films/index.php"><font size=1>[Home]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/paper.php"><font size=1>[History]</font></a></b>


<b><a href="http://projects.csail.mit.edu/films/searchPage.php"><font size=1>[Search]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/final/timelinewithformat.php"><font size=1>[Timeline]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/numberedIndexPage.php"><font size=1>[By Number]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/chronicledIndexPage.php"><font size=1>[By Year]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/categorizedIndexPage.php"><font size=1>[By Category]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/podcastindex.php"><font size=1>[Podcasts]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/oralhist.php"><font size=1>[Oral Histories]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/sources.php"><font size=1>[Links]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/textintro.php"><font size=1>[Text]</font></a></b>


</center>

<p><table text-align="center" align="center">
<tbody>
<tr>

<td>
<center>
<p><b>Cube Manipulation and Chess</b>
<br><b>Early Simulations and Graphics</b>
<br><b>The Turtle Robots and Logo</b>
<br><b>The Minsky-Bennett Arm</b>
<br><b>Bongo Board Balance Research</b>
<br><b>Various Robots</b>
<br><b>Screenshots</b>
<br><b>Math</b>

<p><b>Cube Manipulation and Chess</b>
<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=63">63-chess</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=04">04-cubechess</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=13">13-cubechess</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=14">14-cube</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=19">19-cube</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=20">20-cube</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=33">33-cube</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=51">51-cube</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=78">78-cube</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=80">80-cube</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=87">87-cube</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=93">93-cube</a>




<p><b>Early Simulations and Graphics</b>
<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=05">05-life</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=03">03-gas</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=30">30-gas</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=16">16-solar</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=23">23-worm</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=24">24-worm</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=27">27-solar</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=29">29-solar</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=36">36-wireforks</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=39">39-worm</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=48">48-worm</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=49">49-worm</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=55">55-worm</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=56">56-life</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=65">65-worm</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=66">66-worm</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=67">67-life</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=68">68-gas</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=69">69-gas</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=70">70-gas</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=72">72-gas</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=73">73-gas</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=77">77-life</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=90">90-worm</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=94">94-solar</a>




<p><b>The Minsky-Bennett Arm</b>
<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=07">07-arm</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=43">43-arm</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=61">61-arm</a>


<p><b>The Turtle Robots and Logo</b>
<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=08">08-turtle</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=17">17-turtle</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=22">22-turtle</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=25">25-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=28">28-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=40">40-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=44">44-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=50">50-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=58">58-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=62">62-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=75">75-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=79">79-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=83">83-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=89">89-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=90">90-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=91">91-logo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=92">92-turtle</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=95">95-turtle</a>

<p><b>Bongo Board Balance Research</b>
<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=18">18-bongo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=21">21-bongo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=54">54-bongo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=59">59-bongo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=60">60-bongo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=74">74-bongo</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=86">86-bongo</a>


<p><b>Various Robots</b>
<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=01">01-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=02">02-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=06">06-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=09">09-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=10">10-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=11">11-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=12">12-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=15">15-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=26">26-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=31">31-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=34">34-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=37">37-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=38">38-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=41">41-radial</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=42">42-radial</a>


<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=45">45-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=46">46-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=47">47-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=52">52-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=53">53-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=64">64-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=71">71-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=76">76-robot</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=84">84-robot</a>


<p><b>Screenshots</b>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=35">35-screen</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=57">57-screen</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=82">82-screen</a>


<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=85">85-screen</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=88">88-eye</a>

<p><b>Math</b>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=32">32-mathlab</a>

<br><a href="http://projects.csail.mit.edu/films/dynamicPage.php?reel%23=81">81-mathlab</a>
</center>
</td>
</tr>
</tbody>

</table>
<br>


<center>
<b><a href="http://projects.csail.mit.edu/films/index.php"><font size=1>[Home]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/paper.php"><font size=1>[History]</font></a></b>


<b><a href="http://projects.csail.mit.edu/films/searchPage.php"><font size=1>[Search]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/final/timelinewithformat.php"><font size=1>[Timeline]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/numberedIndexPage.php"><font size=1>[By Number]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/chronicledIndexPage.php"><font size=1>[By Year]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/categorizedIndexPage.php"><font size=1>[By Category]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/podcastindex.php"><font size=1>[Podcasts]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/oralhist.php"><font size=1>[Oral Histories]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/sources.php"><font size=1>[Links]</font></a></b>

<b><a href="http://projects.csail.mit.edu/films/textintro.php"><font size=1>[Text]</font></a></b>


</center>


			<br>
			<table width="100%" text-align="center" align="center">
			<tbody>	
				<tr>
					<td width="47%">
						<div align="right">
						<a href="http://web.mit.edu"><img border="0" src="http://web.mit.edu/img/d060504-logo.gif"></a>
						</div>
					</td>
					<td width="20">
					</td>
					<td width="20">
					</td>
					<td>
						<div align="left">
						<a href="http://nsf.gov"><img border="0" src="http://projects.csail.mit.edu/films/nsf.gif"></a>
						</div>
					</td>
				</tr>
			</tbody>
			</table>
			<center> 
			<a href="mailto:tjg@csail.mit.edu"><font size="1" color="black">TJG</font></a>
			</center>
		</td>
	</tr>
</tbody>
</table>

</body>

</html>
