<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	
  <meta name="description" content="MIT CSAIL Film History of AI Database">

	
  <meta name="keywords" content="CSAIL, MIT, MIT CSAIL, Film History of AI, History of AI, AI, MIT AI, Artificial 
Intelligence">

	
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	
	
  <title>MIT Film History of Artificial Intelligence</title>
  <link rel="stylesheet" type="text/css" href="style.css">
  <style type="text/css">
<!--
.style1 {font-size: 100%}
#triangleLayer {
	position:absolute;
	width:70%;
	height:60%;
	z-index:1;
	left: 36px;
	top: 208px;
	overflow: visible;
	background-color: #FFFFFF;
	visibility: visible;
}
-->
  </style>
</head>


<body>

<span class="style1">
<style type = "text/css">

body {
background-color: #FFFFFF;
font-family: Arial, Verdana, sans-serif;
font-size: 18px;
color: #000000;
scrollbar-face-color: #808080;
scrollbar-highlight-color: #FF8C00;
scrollbar-3dlight-color: #FFFF00;
scrollbar-darkshadow-color: #000000;
scrollbar-shadow-color: #FFFFFF;
scrollbar-arrow-color: #800080;
scrollbar-track-color: #FFFFE0;
}

a { font-family: Arial, Verdana, sans-serif; font-size: 14px; color: #483D8B; text-decoration: underline}

a:hover { font-family: Arial, Verdana, sans-serif; font-size: 14px; color: #000000; background-color: #FFFFFF}

h1 { font-family: Arial, Verdana, sans-serif; font-size: 26px; color: #000000 }
h2 { font-family: Arial, Verdana, sans-serif; font-size: 22px; color: #000000 }
h3 { font-family: Arial, Verdana, sans-serif; font-size: 20px; color: #000000 }
h4 { font-family: Arial, Verdana, sans-serif; font-size: 18px; color: #000000 }
h5 { font-family: Arial, Verdana, sans-serif; font-size: 12px; color: #000000 }
h6 { font-family: Arial, Verdana, sans-serif; font-size: 8px; color: #A52A2A }

hr{ color:brown; background-color:#FFFFFF; width:90%; height:2px; }

table { font-family: Arial, Verdana, sans-serif; font-size: 14px; color: #000000; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px}

.note { font-family: Arial, Verdana, sans-serif; font-size: 14px; color: #800080;
font-weight: bold;}
head.html

<table height="80" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="left" valign="bottom" width="160" rowspan="2">
			<a href="http://www.ibdhost.com/"><img src="free/images/logo.gif" width="160" height="74" border="0" alt="logo"></a>
		</td>
		<td align="left" valign="bottom" height="25" width="100%" nowrap colspan="4">
			<b>Title or Links Here</b>
		</td>
	</tr>
	<tr>
		<td align="right" valign="top" height="50" width="100%" nowrap colspan="4">
			<table width="455" height="50" cellspacing="0" cellpadding="0" border="0" background="free/images/headcolor.gif">
				<tr>
					<td height="50" width="55" align="left" valign="top" background="free/images/headback.gif">
						<img src="free/images/headback.gif" width="55" height="50" border="0" alt="background">
					</td>
					<td height="50" width="400" align="center">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top" height="5" background="free/images/headcolor.gif" nowrap colspan="5">
			<table width="100%" height="1" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<img src="free/images/headcolor.gif" width="1" height="1" alt="bar">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</style>
<!-- Table for Main Body -->
<br>
</span>
<table border="0" cellpadding="2" cellspacing="0" height="100%" width="100%">

	<tbody>
    <tr>

		

		<th width="1" rowspan="2" valign="left" bgcolor="#cc0033" class="style1">
		
<center>
<br><br><br><p><a href="index.php"><font size=1 color="#D3D3D3">Home</font></a>

<p><b><a href="paper.php"><font size=1 color="#D3D3D3">History</font></a></b>

<p><b><a href="searchPage.php"><font size=1 color="#D3D3D3">Search</font></a></b>

<p><b><a href="final/timelinewithformat.php"><font size=1 color="#D3D3D3">Timeline</font></a></b>

<p><b><a href="numberedIndexPage.php"><font size=1 color="#D3D3D3">By Number</font></a></b>

<p><b><a href="chronicledIndexPage.php"><font size=1 color="#D3D3D3">By Year</font></a></b>

<p><b><a href="categorizedIndexPage.php"><font size=1 color="#D3D3D3">By Category</font></a></b>

<p><b><a href="podcastindex.php"><font size=1 color="#D3D3D3">Podcasts</font></a></b>

<p><b><a href="oralhist.php"><font size=1 color="#D3D3D3">Oral Histories</font></a></b>

<br><p><b><a href="sources.php"><font size=1 color="#D3D3D3">Links</font></a></b>

<p><b><a href="textintro.php"><font size=1 color="#D3D3D3">Text</font></a></b>

</center>

</th>


		<td bgcolor="#808080" class="style1">
			
      <center>
				
      <h1> <font ="verdana"> MIT Film History of Artificial Intelligence</font> </h1>
		  </center>		</td>
	</tr>

	<tr>
		<td valign="top" class="style1">
		<center>
  <table width="100%" height="100%" border="0">
    <tr>
      <td width="75%"><center>
			<div id="triangleLayer">		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
#Layer1 {	position:absolute;
	width:242px;
	height:43px;
	z-index:1;
	left: 90px;
	top: 267px;
	background-color: #FFFFFF;
}
-->
</style></head>

<body>
<table width="100%" border="0">
  <tr>
    <td bordercolor="#FFFFFF" bgcolor="#FFFFFF"><div id="layer1"></div></td>
    <td bordercolor="#FFFFFF" bgcolor="#FFFFFF"><div id="layer2"></div></td>
    <td bordercolor="#FFFFFF" bgcolor="#FFFFFF"><div id="layer3"></div></td>
    <td bordercolor="#FFFFFF" bgcolor="#FFFFFF"><div id="layer4"></div></td>
    <td bordercolor="#FFFFFF" bgcolor="#FFFFFF"><div id="layer5"></div></td>
  </tr>
</table>
</body>
</html>
	</div>
				  
      </center></td>
      
	  <td width="25%"><img
src="http://projects.csail.mit.edu/films/TEST/luis2.jpg" alt="badsouce file" name="b1" width="100%" height="100%" border="0" /></td>
    </tr>
  </table>
</center>
			<br>
</td>
    </tr>








    <tr>

		

		<th width="1" rowspan="2" valign="left" bgcolor="#cc0033" class="style1">		</th>


		<td bgcolor="#808080" class="style1">
					</td>
	</tr>
  </tbody>
</table>
<table align="center" width="100%">

				<tbody>
    <tr>

					<th width="47%" class="style1">
						
      <div align="right">
						<a href="http://web.mit.edu"><img src="http://web.mit.edu/img/d060504-logo.gif" border="0"></a>					  </div>					</th>

					<th width="20" class="style1">					</th>

					<th width="20" class="style1">					</th>

					<th class="style1">
						
      <div align="left">
						<a href="http://nsf.gov"><img src="http://www.soest.hawaii.edu/seagrant/MSURF_website/www/firework/nsf.gif" border="0"> </a>					  </div>					</th>
				</tr>
  </tbody>
</table>

			
<center> 
			<span class="style1"><a href="mailto:tjg@csail.mit.edu"><font color="black">TJG</font></a></span>
</center>
</body>
</html>
